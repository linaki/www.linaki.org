var direction=0;
var writing="";
var keytaps=["making", "3D modeling", "retrogaming", "web-designing", "creating", "inventing", "innovating", "debugging", "optimizing"]
keytap=document.getElementById("keytap");
function reverseDirection() {
    if (direction==1) {
        direction=0;
    } else {
        writing=keytaps[Math.floor(Math.random() * keytaps.length)];
        direction=1;
   }
}
function updateKeytap() {
    if (direction==0) {
    keytap.innerText=keytap.innerText.slice(0, -1);
        if (keytap.innerText.length==1) {
            setTimeout(reverseDirection, 3000);
        }
  } else {
        keytap.innerText+=writing.slice(0, 1);
        writing=writing.substring(1, writing.length)
        if (writing.length==1) {
            setTimeout(reverseDirection, 2000);
        }
    }
}
setInterval(updateKeytap, 100);